import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {LoginCredentials} from '../models/login-credentials';
import {Observable} from 'rxjs';
import {LoginResponse} from '../models/login-response';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  readonly loginUrl = `${environment.baseUrl}/auth`;
  constructor(private httpClient: HttpClient) { }
  login(credentials: LoginCredentials): Observable<LoginResponse> {
    console.log(`Logging in ${this.loginUrl}`);
    return this.httpClient.post<LoginResponse>(this.loginUrl, credentials);
  }
  isLoggedIn(): boolean {
    const token = localStorage.getItem('token');
    return token != null && token.length !== 0;
  }
}
