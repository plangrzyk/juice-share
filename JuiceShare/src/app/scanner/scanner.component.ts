import {Component, OnInit, ViewChild} from '@angular/core';
import {ZXingScannerComponent} from '@zxing/ngx-scanner';

@Component({
  selector: 'app-scanner',
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss']
})
export class ScannerComponent implements OnInit {

  @ViewChild('scanner', { static: true }) scanner: ZXingScannerComponent;
  currentDevice: MediaDeviceInfo;

  constructor() { }

  ngOnInit() {
    this.scanner.camerasFound.subscribe((devices: MediaDeviceInfo[]) => {
      this.currentDevice = devices[0];
    });
  }

  handleReadResult(response: String) {
    console.log(response);
    this.scanner.enable = false;
  }

}
