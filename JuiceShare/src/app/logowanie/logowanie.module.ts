import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogowanieRoutingModule } from './logowanie-routing.module';
import { LogowanieComponent } from './logowanie.component';
import { SignUpCard1Component } from './sign-up-card1/sign-up-card1.component';
import { SignUpForm1Component } from './sign-up-form1/sign-up-form1.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [LogowanieComponent, SignUpCard1Component, SignUpForm1Component],
  imports: [
    CommonModule,
    LogowanieRoutingModule,
    SharedModule
  ]
})
export class LogowanieModule { }
