import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-logowanie',
  templateUrl: './logowanie.component.html',
  styleUrls: ['./logowanie.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogowanieComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
