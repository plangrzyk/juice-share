import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {LoginCredentials} from '../../shared/models/login-credentials';
import {LoginResponse} from '../../shared/models/login-response';
import {LoginService} from '../../shared/services/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-up-card1',
  templateUrl: './sign-up-card1.component.html',
  styleUrls: ['./sign-up-card1.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignUpCard1Component implements OnInit {

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }
  login(username: string, password: string) {
    const credentials = new LoginCredentials();
    credentials.username = username;
    credentials.password = password;
    console.log(`requesting login for ${username}, ${password}`);
    this.loginService.login(credentials).subscribe((token: LoginResponse) => {
      localStorage.setItem('token', token.token);
      this.router.navigate(['scan']);
    }, (e) => {
      console.log(e);
    });
  }
}
