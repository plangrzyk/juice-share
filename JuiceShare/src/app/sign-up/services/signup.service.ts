import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SignupDto} from '../models/signup-dto';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable()
export class SignupService {
  registerUrl = `${environment.baseUrl}/registration`;
  constructor(private httpClient: HttpClient) { }
  signUp(signupDto: SignupDto): Observable<any> {
    return this.httpClient.post(this.registerUrl, signupDto);
  }
}
