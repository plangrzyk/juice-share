export class SignupDto {
    firstName: String;
    lastName: String;
    login: String;
    password: String;
}
