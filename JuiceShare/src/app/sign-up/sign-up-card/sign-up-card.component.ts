import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {SignupService} from '../services/signup.service';
import {SignupDto} from '../models/signup-dto';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-up-card',
  templateUrl: './sign-up-card.component.html',
  styleUrls: ['./sign-up-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignUpCardComponent implements OnInit {

  constructor(private signupService: SignupService, private router: Router) { }

  ngOnInit() {
  }
  register(name: String, surname: String, email: String, password: String) {
    const signupDto = new SignupDto();
    signupDto.firstName = name;
    signupDto.lastName = surname;
    signupDto.login = email;
    signupDto.password = password;
    console.log(this.signupService.registerUrl);
    this.signupService.signUp(signupDto).subscribe(() => {this.router.navigate(['Logowanie']); },
      (e) => {
        console.log(e);
        console.log('Registration failed');
      });
  }

}
