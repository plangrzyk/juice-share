import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';


@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html'
})
export class PhotoComponent implements OnInit, AfterViewInit {

  @ViewChild('player', {static: false})
  player: ElementRef;
  @ViewChild('capture', {static: true})
  capture: ElementRef;
  photoTaken = false;
  data: string;


  constructor() { }

  ngOnInit() {
    // this.initStream();
  }

  ngAfterViewInit() {
    this.initStream();
  }

  initStream() {
    const config = {video: true, audio: false};
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia(config))) {
      navigator.mediaDevices.getUserMedia(config).then(stream => {
        this.player.nativeElement.srcObject = stream;
        this.player.nativeElement.play();
      });
    }
  }

  takePhoto() {
    const ctx = this.capture.nativeElement.getContext('2d');
    ctx.drawImage(this.player.nativeElement, 0, 0, 640, 480);
    this.data = this.capture.nativeElement.toDataURL('image/png');
    this.player.nativeElement.pause();
    this.photoTaken = true;
  }

  downloadPhoto() {
    const link = document.createElement('a');
    link.href = this.data;
    link.setAttribute('download', 'img.png');
    link.click();
  }

}
