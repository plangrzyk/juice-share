import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import {ScannerComponent} from './scanner/scanner.component';
import {PhotoComponent} from './photo/photo.component';

const routes: Routes = [
  { path: 'Sign-in', loadChildren: './sign-up/sign-up.module#SignUpModule' },
  { path: 'Logowanie', loadChildren: './logowanie/logowanie.module#LogowanieModule' },
  { path: 'scan', component: ScannerComponent },
  { path: 'photo', component: PhotoComponent },
  { path: '', redirectTo: 'Sign-in', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
