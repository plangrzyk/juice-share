#/usr/bin/env bash

if [[ $1 == "--help" ]]
then
	echo "To be added to crontab. Script checks for update in project git repository(master branch), pulls changes and reload application server"
	exit
fi

logFile="${Log_dir}/autoUpdate.logs"
tmpFile='/tmp/projectUdater.tmp'

git_command(){
	while [[ "${1}" ]]; do
		args="${args} $1"
		shift
	done
	eval "git --git-dir=${Project_dir}/.git ${args}"
}

git_command --no-pager fetch --dry-run -v &> ${tmpFile}

if [[ $(cat ${tmpFile} | grep 'origin/master' | grep '[up to date]') ]]; then
	echo "$(date) : No update needed" >> ${logFile}
	exit
fi

git_command pull

echo "${date} : Updated. Last commit is\n$(git_command log -1 --pretty=%B)" >> ${logFile}

${Project_dir}/initProject.sh
