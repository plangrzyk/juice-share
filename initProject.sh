customProjectDir="${1}"

if [[ -z ${customProjectDir} ]]; then
	source .config
else
	source ${customProjectDir}/.config
fi

if [[ ! $(docker ps | grep mongodb) ]]; then
	docker-compose -f db/docker-compose.yml up -d
fi

npm install --prefix ${Project_dir}/JuiceShare
npm run-script build --prefix ${Project_dir}/JuiceShare
sudo rm -rf /var/www/html
sudo cp -r ${Project_dir}/JuiceShare/dist/JuiceShare /var/www/html

mvn compile -f ${Project_dir}/Backend
mvn package -f ${Project_dir}/Backend

kill -9 `ps aux | grep -E " +java -jar ${Project_dir}/Backend/target/.*.jar" | sed -E 's/([a-z]+ +)([0-9]+)( +.*)/\2/g'`
java -jar ${Project_dir}/Backend/target/*.jar &
