package pl.edu.pwr.juiceshare.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "chargers")
class Charger(name: String, owner: String, maximumPower: Int, outletType: String, stationsAmount: Int, state: ChargerStates) {
    @Id
    var id: String? = null

    var name: String = name
    var owner: String = owner
    var maximumPower: Int = maximumPower
    var outletType: String = outletType
    var stationsAmount: Int = stationsAmount
    var state: ChargerStates = state
    var qrCode: ByteArray? = null
}
