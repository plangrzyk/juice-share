package pl.edu.pwr.juiceshare.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pl.edu.pwr.juiceshare.models.Charger
import pl.edu.pwr.juiceshare.models.ChargerStates
import pl.edu.pwr.juiceshare.repositories.ChargerRepository
import pl.edu.pwr.juiceshare.utils.ImageUtils
import pl.edu.pwr.juiceshare.utils.QRCodesUtils
import java.awt.image.BufferedImage


@Service
class ChargerService(chargerRepository: ChargerRepository, distributionSpotService: DistributionSpotService, qrCodesUtils: QRCodesUtils, imageUtils: ImageUtils) {
    @Autowired
    val chargerRepository = chargerRepository

    @Autowired
    val distributionSpotService = distributionSpotService

    @Autowired
    val qrCodesUtils: QRCodesUtils = qrCodesUtils

    @Autowired
    val imageUtils: ImageUtils = imageUtils

    fun add(name: String, owner: String, maximumPower: Int, outletType: String, stationsAmount: Int){
        var initialState = ChargerStates.DEACTIVATED
        var charger = Charger(name, owner, maximumPower, outletType, stationsAmount, initialState)
        chargerRepository.save(charger)
        charger.qrCode = generateQRcode(charger)
        chargerRepository.save(charger)
    }

    fun getByUser(owner: String): List<Charger> {
        var chargers: List<Charger> = chargerRepository.findByOwner(owner)
        return chargers
    }

    fun getChargerQRcodeStreamingURI(id: String): String {
        var charger: Charger = chargerRepository.findBy_Id(id)
        var qrImage = charger.qrCode
        var uri = imageUtils.convert_ByteArray_To_StreamingURI(qrImage!!)
        return uri
    }

    fun getAll(): List<Charger> {
        var chargers: List<Charger> = chargerRepository.findAll()
        return chargers
    }

    fun safeRemoveById(id: String){
        updateState(id, ChargerStates.DEACTIVATED)
        removeRelatedDistributionSpots(id)
    }

    fun forceRemoveById(id: String){
        removeRelatedDistributionSpots(id)
        removeById(id)
    }

    fun removeRelatedDistributionSpots(id: String){
        var distributionSpotIds = distributionSpotService.getDistributionSpotsIdsByCharger(id)
        println("Removing distribution spots for charger " + id)
        if(distributionSpotIds.size == 0){
            println("There are no distribution spots for charger " + id)
            return
        }
        for (ds in distributionSpotIds){
            distributionSpotService.removeDistributionSpot(ds)
        }
        println("Successfully removed all distribution spots for charger " + id)
    }


    fun removeById(id: String){
        var charger: Charger = chargerRepository.findBy_Id(id)
        chargerRepository.delete(charger)
    }

    fun generateQRcode(charger: Charger): ByteArray {
        println("Generating QR code for charger")
        var chargerQRdata: String = "${charger.id}"
        var qrCode: BufferedImage = qrCodesUtils.generateQRCodeImage(chargerQRdata)!!
        var byteStream: ByteArray = imageUtils.convert_BufferedImage_To_ByteArray(qrCode)
        return byteStream
    }

    fun updateState(id: String, newState: ChargerStates) {
        var charger = chargerRepository.findBy_Id(id)
        charger.state = newState
        chargerRepository.save(charger)
        println("Successfully changed charger state to " + newState)
    }

    fun updateState(id: String, state: String){
        try{
            var newState = ChargerStates.valueOf(state.toUpperCase())
            updateState(id, newState)
        } catch (e: IllegalArgumentException){
            println("Chosen charger state is incorrect!")
            return
        }
    }
}
