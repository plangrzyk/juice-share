package pl.edu.pwr.juiceshare.utils

import com.google.zxing.BarcodeFormat
import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import org.springframework.stereotype.Component
import java.awt.image.BufferedImage

@Component
class QRCodesUtils {
    fun generateQRCodeImage(barcodeText: String?): BufferedImage? {
        println("QR code generation started for data: " + barcodeText)
        val barcodeWriter = QRCodeWriter()
        val bitMatrix: BitMatrix = barcodeWriter.encode(barcodeText, BarcodeFormat.QR_CODE, 200, 200)
        println("Successfully generated QR code")
        return MatrixToImageWriter.toBufferedImage(bitMatrix)
    }
}