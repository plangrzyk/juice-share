package pl.edu.pwr.juiceshare.models

class Credentials(login: String?, passwordHash: String?) {
    var login : String? = login
    var passwordHash : String? = passwordHash;
}