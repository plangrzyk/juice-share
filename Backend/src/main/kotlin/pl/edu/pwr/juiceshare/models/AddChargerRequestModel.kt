package pl.edu.pwr.juiceshare.models

class AddChargerRequestModel(name: String, maximumPower: Int, outletType: String, stationsAmount: Int) {
    val name: String = name
    val maximumPower: Int = maximumPower
    val outletType: String = outletType
    val stationsAmount: Int = stationsAmount
}