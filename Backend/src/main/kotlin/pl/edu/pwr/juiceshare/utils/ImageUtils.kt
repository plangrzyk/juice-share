package pl.edu.pwr.juiceshare.utils

import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO
import javax.xml.bind.DatatypeConverter


@Component
class ImageUtils {
    fun convert_BufferedImage_To_ByteArray(bufferedImage: BufferedImage): ByteArray {
        val outputStream = ByteArrayOutputStream()
        ImageIO.write(bufferedImage, "jpg", outputStream)
        val imageBytes = outputStream.toByteArray()
        return imageBytes
    }

    fun convert_ByteArray_To_StreamingURI(bytes: ByteArray) : String {
        var mime : String = ""
        val base64 = DatatypeConverter.printBase64Binary(bytes)
        println("Converted to Streaming URI data:")
        return "data:$mime;base64,$base64"
    }

    fun convert_MultipartFile_to_ByteArray(file: MultipartFile): ByteArray {
        var imageByteArray = file.bytes
        return imageByteArray
    }
}
