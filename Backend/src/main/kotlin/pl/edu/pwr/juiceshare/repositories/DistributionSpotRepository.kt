package pl.edu.pwr.juiceshare.repositories


import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import pl.edu.pwr.juiceshare.models.DistributionSpot

interface DistributionSpotRepository : MongoRepository<DistributionSpot, String> {
    @Query("{ 'chargerID' : ?0 }")
    fun findByChargerID(chargerID: String?): List<DistributionSpot>

    @Query("{ 'id' : ?0 }")
    fun findBy_Id(id: String?): DistributionSpot

}
