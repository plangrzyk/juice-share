package pl.edu.pwr.juiceshare.utils

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.util.*
import kotlin.collections.HashMap

@Component
class JwtTokenUtils {

    val TOKEN_VALIDITY_TIME : Long = 60 * 60
    val secret : String = "98a3fd87a"

    fun getUsername(token : String) : String {
        return getClaimFromToken(token, Claims::getSubject)
    }

    fun getExpirationDate(token: String) : Date {
        return getClaimFromToken(token, Claims::getExpiration)
    }

    fun <T> getClaimFromToken(token: String, claimsResolver : Function1<Claims, T>) : T {
        val claims : Claims = getAllClaimsFromToken(token)
        return claimsResolver.invoke(claims)
    }

    private fun getAllClaimsFromToken(token : String) : Claims {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .body
    }

    private fun isTokenExpired(token : String) : Boolean {
        val expiration : Date = getExpirationDate(token)
        return expiration.before(Date())
    }

    fun generateToken(userDetails : UserDetails) : String {
        val claims : HashMap<String, Any> = HashMap()
        val subject : String = userDetails.username
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(Date())
                .setExpiration(Date(System.currentTimeMillis() + TOKEN_VALIDITY_TIME * 1000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact()
    }

    fun validateToken(token : String, userDetails: UserDetails) : Boolean {
        val username = getUsername(token)
        return (username == userDetails.username && !isTokenExpired(token))
    }

}