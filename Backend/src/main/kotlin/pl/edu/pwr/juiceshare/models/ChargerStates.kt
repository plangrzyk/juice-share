package pl.edu.pwr.juiceshare.models

interface IChargerStates {
    override fun toString(): String
}

enum class ChargerStates() {
    ACTIVE{
        override fun toString(): String = "active"
    },
    SERVICING{
        override fun toString(): String = "servicing"
    },
    DEACTIVATED{
        override fun toString(): String = "deactivated"
    },
    REMOVED{
        override fun toString(): String = "removed"
    }
}