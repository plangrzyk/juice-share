package pl.edu.pwr.juiceshare.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "distributionSpots")
class DistributionSpot(x: Double, y: Double, description: String?, chargerID: String?) {
    @Id
    var id: String? = null

    var x: Double = x
    var y: Double = y
    var description: String? = description
    var chargerID: String? = chargerID
    var image: ByteArray? = null
}
