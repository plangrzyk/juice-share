package pl.edu.pwr.juiceshare.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import pl.edu.pwr.juiceshare.models.DistributionSpot
import pl.edu.pwr.juiceshare.repositories.DistributionSpotRepository
import pl.edu.pwr.juiceshare.utils.ImageUtils

@Service
class DistributionSpotService(distributionSpotRepository: DistributionSpotRepository, imageUtils: ImageUtils){
    @Autowired
    val distributionSpotRepository: DistributionSpotRepository = distributionSpotRepository

    @Autowired
    val imageUtils: ImageUtils = imageUtils

    fun addDistribution(distributionSpot: DistributionSpot) {
        distributionSpotRepository.save(distributionSpot)
        //miejsce na sprawdzenie czy dany charger istnieje (i jest aktywowany?)
    }

    fun getDistributionSpotByChargerID(id:String): List<DistributionSpot> {
        return distributionSpotRepository.findByChargerID(id)
    }

    fun removeDistributionSpot(id: String) {
        var distributionSpot: DistributionSpot = distributionSpotRepository.findBy_Id(id)
        distributionSpotRepository.delete(distributionSpot)
        println("Successfully removed distribution spot " + id)
    }

    fun getDistributionSpotsIdsByCharger(chargerId: String): List<String> {
        var ds_list: List<DistributionSpot> = getDistributionSpotByChargerID(chargerId)
        var ids: MutableList<String> = mutableListOf()
        for (ds in ds_list){
            ids.add(ds.id!!)
        }
        return ids.toList()
    }


    fun uploadDistributionSpotImage(id: String, file: MultipartFile){
        var distributionSpot: DistributionSpot = distributionSpotRepository.findBy_Id(id)
        var byteArrayImage = imageUtils.convert_MultipartFile_to_ByteArray(file)
        distributionSpot.image = byteArrayImage
        distributionSpotRepository.save(distributionSpot)
    }
}
