package pl.edu.pwr.juiceshare.controllers

import pl.edu.pwr.juiceshare.models.JwtRequest
import pl.edu.pwr.juiceshare.models.JwtResponse
import pl.edu.pwr.juiceshare.services.AuthUserDetailsService
import pl.edu.pwr.juiceshare.utils.JwtTokenUtils
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin
@RequestMapping("/auth")
class AuthenticationController(authenticationManager: AuthenticationManager, jwtTokenUtils: JwtTokenUtils,
                               userDetails : AuthUserDetailsService) {

    val authenticationManager = authenticationManager
    val jwtTokenUtils = jwtTokenUtils
    val userDetailsService = userDetails


    @PostMapping
    fun createToken(@RequestBody details : JwtRequest) : ResponseEntity<JwtResponse> {
        authenticationManager.authenticate(UsernamePasswordAuthenticationToken(details.username, details.password))
        val userDetails = userDetailsService.loadUserByUsername(details.username)
        return ResponseEntity.ok(JwtResponse(jwtTokenUtils.generateToken(userDetails)))
    }

}