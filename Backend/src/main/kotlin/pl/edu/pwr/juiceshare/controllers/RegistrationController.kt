package pl.edu.pwr.juiceshare.controllers

import pl.edu.pwr.juiceshare.models.Person
import pl.edu.pwr.juiceshare.services.PersonService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import pl.edu.pwr.juiceshare.models.Credentials
import pl.edu.pwr.juiceshare.models.RegistrationRequestModel
import pl.edu.pwr.juiceshare.services.CredentialsService

@CrossOrigin
@RestController
@RequestMapping("/registration")
class RegistrationController(personService: PersonService, credentialsService: CredentialsService) {

    @Autowired
    var personService: PersonService = personService
    var credentialsService: CredentialsService = credentialsService

    @PostMapping
    fun register(@RequestBody data: RegistrationRequestModel) {
        println("Registering " + data.firstName + " " + data.lastName + " as " + data.login)
        val creds = Credentials(data.login, credentialsService.hashIt(data.password))
        val person = Person(data.login, data.firstName, data.lastName, creds)
        if(personService.checkIfLoginExists(data.login!!)){
            println("User with such login already exists!")
            return
        }
        personService.addPerson(person)
        println("Succesfully registered user " + data.login)
    }
}