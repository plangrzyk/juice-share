package pl.edu.pwr.juiceshare.models

class RegistrationRequestModel(login: String?, firstName: String?, lastName: String?, password: String?) {
    val login: String? = login
    val firstName: String? = firstName
    val lastName: String? = lastName
    val password: String? = password
}