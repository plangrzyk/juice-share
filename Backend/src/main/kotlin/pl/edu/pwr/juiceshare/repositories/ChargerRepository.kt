package pl.edu.pwr.juiceshare.repositories

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository
import pl.edu.pwr.juiceshare.models.Charger

@Repository
interface ChargerRepository : MongoRepository<Charger, String>{
    @Query("{ 'owner' : ?0 }")
    fun findByOwner(owner: String?): List<Charger>

    @Query("{ 'id' : ?0 }")
    fun findBy_Id(id: String?): Charger
}
