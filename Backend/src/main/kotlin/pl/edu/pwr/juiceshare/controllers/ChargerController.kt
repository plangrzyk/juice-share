package pl.edu.pwr.juiceshare.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import pl.edu.pwr.juiceshare.models.*
import pl.edu.pwr.juiceshare.services.ChargerService

@RestController
@RequestMapping("/Charger")
class ChargerController(chargerService: ChargerService) {

    @Autowired
    var chargerService: ChargerService = chargerService

    @PostMapping("add")
    fun addCharger(@RequestBody data: AddChargerRequestModel) {
        println("Checking current user")
        var owner: Person = Person("login", "firstName", "lastName", Credentials("login", "hash"))
//ToDo: Zmienić to na CurrentUsera. Jak my w sumie identyfikujemy zalogowanych userów?
//        var owner: Person = getCurrentUser()
        println("Adding charger to user " + owner.login)
        chargerService.add(data.name, owner.login!!, data.maximumPower, data.outletType, data.stationsAmount)
        println("Successfully added new charger " + data.name)
    }

    @GetMapping("get/byowner/{owner}")
    fun getUserChargers(@PathVariable owner: String): List<Charger> {
        println("Getting chargers for user " + owner)
        var chargers: List<Charger> = chargerService.getByUser(owner)
        println(chargers)
        return chargers
    }

    @GetMapping("get/all")
    fun getAllChargers(): List<Charger> {
        println("Getting all chargers")
        var chargers: List<Charger> = chargerService.getAll()
        println("Chargers: " + chargers)
        return chargers
    }

    @PostMapping("remove/byid/{id}")
    fun removeCharger(@PathVariable id: String) {
        println("Safety removing charger with id = " + id)
        chargerService.safeRemoveById(id)
        println("Successfully removed all charger related DistributionSpots and deactivated charger")
    }

    @PostMapping("forceRemove/byid/{id}")
    fun forceRemoveCharger(@PathVariable id: String) {
        println("Force removing charger with id = " + id)
        chargerService.forceRemoveById(id)
        println("Successfully completely removed charger and all related DistributionSpots")
    }

    @GetMapping("get/byid/{id}/QRcode")
    fun getChargerQRcode(@PathVariable id: String): String {
        println("Retrieving QR code for charger with id = " + id)
        var uri = chargerService.getChargerQRcodeStreamingURI(id)
        println("Successfully retrieved charger QR code")
        return uri
    }

    @PostMapping("{id}/changeState/{state}")
    fun updateChargerState(@PathVariable id: String, @PathVariable state: String){
        println("Changing state of charger " + id + " to " + state)
        chargerService.updateState(id, state)
    }
}
