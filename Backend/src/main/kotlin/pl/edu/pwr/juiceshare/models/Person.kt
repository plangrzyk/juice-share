package pl.edu.pwr.juiceshare.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "users")
class Person(login: String?, firstName: String?, lastName: String?, credentials: Credentials) {
    @Id
    var id: String? = null
    var login: String? = login
    var firstName: String? = firstName
    var lastName: String? = lastName
    var credentials: Credentials? = credentials

    fun checkHash(hash: String): Boolean{
        return hash == credentials!!.passwordHash
    }
}