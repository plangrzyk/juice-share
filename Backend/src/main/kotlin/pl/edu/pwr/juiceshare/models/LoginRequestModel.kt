package pl.edu.pwr.juiceshare.models

class LoginRequestModel(login: String?, password: String?) {
    val login: String? = login
    val password: String? = password
}