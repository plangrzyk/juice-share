package pl.edu.pwr.juiceshare.models

import org.springframework.web.multipart.MultipartFile

class ImageFileRequestModel(
        val file: MultipartFile? = null,
        val filename: String = "",
        val url: String = ""
)