package pl.edu.pwr.juiceshare.services

import org.springframework.stereotype.Service
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@Service
class CredentialsService{
    fun checkPassword(passwordHash: String, password: String): Boolean{
        return BCryptPasswordEncoder().matches(password, passwordHash)
    }

    fun hashIt(password: String?) : String{
        val passwordEncoder = BCryptPasswordEncoder()
        val hashedPassword: String = passwordEncoder.encode(password)
        return hashedPassword
    }






//    fun addCredentials(credentials: Credentials) {
//        repository.save(credentials)
//    }

//
//    fun addPerson(person: Person) {
//        repository.save(person)
//    }
//
//    fun getAllPersons() : List<Person> {
//        return repository.findAll()
//    }
//
//    fun getPerson(id : String) :Person? {
//        var person : Person? = null
//         repository.findById(id).ifPresent( {p -> person = p})
//
//        return person;
//    }
//
//    fun getPersonByName(name :String) : Person? {
//        var person : Person? = null;
//        repository.findByName(name).ifPresent { p -> person = p }
//        return person
//    }
}
