package pl.edu.pwr.juiceshare.repositories

import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository
import pl.edu.pwr.juiceshare.models.Person
import java.util.*

@Repository
interface PersonRepository : MongoRepository<Person, String> {
    @Query("{ 'login' : ?0 }")
    fun findByLogin(login: String): Person?
}
