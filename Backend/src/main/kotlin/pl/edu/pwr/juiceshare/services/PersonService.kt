package pl.edu.pwr.juiceshare.services

import pl.edu.pwr.juiceshare.models.Person
import pl.edu.pwr.juiceshare.repositories.PersonRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PersonService(personRepository: PersonRepository, credentialsService: CredentialsService) {
        @Autowired
        val repository = personRepository

        @Autowired
        var credentialsService : CredentialsService = credentialsService

        fun addPerson(person: Person) {
            repository.save(person)
        }

        fun getPerson(login: String): Person? {
            if (! checkIfLoginExists(login))
                return null
            return repository.findByLogin(login)
        }

        fun checkIfLoginExists(login: String): Boolean{
            if (repository.findByLogin(login) != null)
                return true
            return false
        }

        fun validateUser(login: String, password: String): Boolean{
            if (! checkIfLoginExists(login)){
                println("Validation: User not found")
                return false
            }
            var person = repository.findByLogin(login)
            if(credentialsService.checkPassword(person!!.credentials!!.passwordHash!!, password)){
                println("Validation: Successfully validated")
                return true
            }
            else{
                println("Validation: Failed to validate user")
                return false
            }
        }
}
