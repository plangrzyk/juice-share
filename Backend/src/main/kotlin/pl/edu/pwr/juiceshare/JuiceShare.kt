package pl.edu.pwr.juiceshare

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class JuiceShare

fun main(args: Array<String>) {
    runApplication<JuiceShare>(*args)
}
