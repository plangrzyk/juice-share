package pl.edu.pwr.juiceshare.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Component
import pl.edu.pwr.juiceshare.repositories.PersonRepository

@Component
class AuthUserDetailsService : UserDetailsService {

    @Autowired
    var personRepository : PersonRepository? = null

    override fun loadUserByUsername(username: String): UserDetails {
        val credentials = personRepository?.findByLogin(username)?.credentials
        return User(credentials?.login, credentials?.passwordHash, ArrayList())
    }
}