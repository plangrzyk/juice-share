package pl.edu.pwr.juiceshare.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import pl.edu.pwr.juiceshare.models.DistributionSpot

import pl.edu.pwr.juiceshare.services.DistributionSpotService

@RestController
@RequestMapping("/distribution_spot")
class DistributionSpotController(distributionSpotService: DistributionSpotService) {

    @Autowired
    var distributionSpotService: DistributionSpotService = distributionSpotService


    @PostMapping
    fun addDistributionSpot(@RequestBody data: DistributionSpot) {
        println("${data.x} ${data.y} ${data.description} ${data.chargerID}")
        distributionSpotService.addDistribution(data)
    }

    @GetMapping("get_spot/{id}")
    fun getByChargerID(@PathVariable id: String) : List<DistributionSpot> {
        return distributionSpotService.getDistributionSpotByChargerID(id)
    }

    @PostMapping("{id}/uploadImage")
    fun loadChargerImage(@RequestParam("file") file: MultipartFile, @PathVariable id: String){
        println("Uploading image for station " + id)
        distributionSpotService.uploadDistributionSpotImage(id, file)
        println("Successfully uploaded station image")
    }
}
