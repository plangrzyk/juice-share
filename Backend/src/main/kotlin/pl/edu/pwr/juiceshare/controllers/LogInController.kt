package pl.edu.pwr.juiceshare.controllers

import pl.edu.pwr.juiceshare.services.CredentialsService
import pl.edu.pwr.juiceshare.services.PersonService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import pl.edu.pwr.juiceshare.models.LoginRequestModel

@RestController
@RequestMapping("/login")
class LogInController(personService: PersonService, credentialsService: CredentialsService) {

    @Autowired
    var personService: PersonService = personService

    @Autowired
    var credentialsService: CredentialsService = credentialsService

    @PostMapping
    fun logIn(@RequestBody data: LoginRequestModel) {
        println("Logging " + data.login)
        if (personService.validateUser(data.login!!, data.password!!)){
            var person = personService.getPerson(data.login)
            println("Succesfully logged user " + person!!.credentials!!.login)
        } else
            println("Password or login incorrect!")
            return

        //odpowiednie requesty
    }


//  RZECZY PONIZEJ JESZCZE SIE PRZYDADZA

//    @GetMapping("all")
//    fun getAll() : List<Person> {
//        //znajdz
//        return service.getAllPersons()
//    }


//    @PostMapping
//    fun savePerson(@RequestBody person : Person) {
//        service.addPerson(person)
//    }


//    @GetMapping("name/{name}")
//    fun getByName(@PathVariable name : String) : Person? {
//        return personService.getPersonByName(name)
//    }

//    @GetMapping("all")
//    fun getAll() : List<Person> {
//        return service.getAllPersons()
//    }
//
//    @GetMapping("/{id}")
//    fun helloMethod(@PathVariable("id") id : String) : Person? {
//        return personService.getPerson(id)
//    }
//
//    @


}
