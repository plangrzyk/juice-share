#!/usr/bin/env bash	

if [[ -z ${1} ]];then
	echo "No mode selected, exiting! Available modes:"
	cat ${BASH_SOURCE} | grep -E '\${mode} == "[a-zA-Z]+"' | sed -E 's/(if \[\[ \(..mode. == ")([a-zA-Z]+)(".*)/\2/g'
	exit
else
	mode=${1}
fi

if [[ (${mode} == "registration") || (${mode} == "all") ]];then 
	curl -X POST http://localhost:8082/registration \
		-H 'content-type: application/json' \
		-d '{
		"login":"kuba.hyzicki@gmail.com",
		"firstName":"Kuba",
		"lastName":"Hyzicki",
		"password":"ciastko"
	}'
fi

if [[ (${mode} == "login") || (${mode} == "all") ]];then 
	curl -X POST http://localhost:8082/login --cookie ./cookie \
		-H 'content-type: application/json' \
		-d '{
		"login":"kuba.hyzicki@gmail.com",
		"password":"ciastko"
	}'
fi

if [[ (${mode} == "logintofail") || (${mode} == "all") ]];then 
	curl -X POST http://localhost:8082/login --cookie ./cookie \
		-H 'content-type: application/json' \
		-d '{
		"login":"kuba.hyzicki@gmail.com",
		"password":"CiastkoOrNotToCiasrko?"
	}'
fi

if [[ (${mode} == "addcharger") || (${mode} == "all") ]];then
	curl -X POST http://localhost:8082/Charger/add \
		-H 'content-type: application/json' \
		-d '{
		"name":"chasd",
		"maximumPower":666,
		"outletType":"ciastkoType",
		"stationsAmount":1
	}'
fi

if [[ (${mode} == "getuserchargers") || (${mode} == "all") ]];then
	curl -X GET http://localhost:8082/Charger/get/byowner/login
fi

if [[ (${mode} == "getallchargers") || (${mode} == "all") ]];then
	curl -X GET http://localhost:8082/Charger/get/all
fi

if [[ (${mode} == "removecharger") || (${mode} == "all") ]];then
	curl -X POST http://localhost:8082/Charger/remove/byid/${2}
fi

if [[ (${mode} == "forceremovecharger") || (${mode} == "all") ]];then
	curl -X POST http://localhost:8082/Charger/forceRemove/byid/${2}
fi

if [[ (${mode} == "getchargerqr") || (${mode} == "all") ]];then
	curl -X GET http://localhost:8082/Charger/get/byid/${2}/QRcode
fi

if [[ (${mode} == "uploadChargerImage") || (${mode} == "all") ]];then
	curl -X POST http://localhost:8082/distribution_spot/${2}/uploadImage \
	-F 'file=@./testImage.jpg' \
	-F 'filename=testImage.jpg'
fi
